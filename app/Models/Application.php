<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'type_id',
        'comment',
        'status',
        'operator_id',
        'brigade_id',
    ];

    public const ACTIVE = 'active';
    public const IN_PROGRESS = 'in_progress';
    public const DONE = 'done';
    public const TAKEN_BY_OPERATOR = 'taken by operator';
    public const TAKEN_BY_TEAM = 'taken by team';

    public const STATUSES = [
        self::ACTIVE,
        self::IN_PROGRESS,
        self::DONE,
    ];


    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function brigade()
    {
        return $this->belongsTo(Brigade::class);
    }
}
